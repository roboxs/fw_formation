#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export PWD='/home/roboxs/fw_formation/build'
export ROS_PACKAGE_PATH='/home/roboxs/fw_formation/src:/home/roboxs/catkin_test_ws/src:/home/roboxs/my_fixed_wing_test/src:/home/roboxs/catkin_ws/src:/opt/ros/melodic/share'